package it.pegaso.cinema.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.pegaso.cinema.model.Film;

public class FilmDao {
	
	private final static String CONNECTION_STRING="jdbc:mysql://localhost:3306/cinema?user=root&password=root";
	
	private Connection connection;
	private PreparedStatement byTitolo;
	private PreparedStatement insert;
	private PreparedStatement update;
	
	public boolean insert(Film f) {
		boolean success= false;
		try {
			getInsert().clearParameters();
			getInsert().setString(1, f.getTitolo());
			getInsert().setString(2, f.getRegista());
			getInsert().setString(3, f.getGenere());
			getInsert().setInt(4, f.getPosti());
			getInsert().setTime(5, f.getOraInizio());
			getInsert().setString(6, f.getDescrizione());
			
			getInsert().execute();
			success = true;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return success;
	}
	
	public boolean update(Film f) {
		boolean success = false;
		try {
			getUpdate().clearParameters();
			getUpdate().setString(1, f.getTitolo());
			getUpdate().setString(2, f.getRegista());
			getUpdate().setString(3, f.getGenere());
			getUpdate().setInt(4, f.getPosti());
			getUpdate().setTime(5, f.getOraInizio());
			getUpdate().setString(6, f.getDescrizione());
			getUpdate().setInt(7, f.getId());
			
			getUpdate().execute();
			success = true;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return success;
	}
	
	
	public Film filmByTitolo(String titolo) {
		
		Film f = null;
		try {
			getByTitolo().clearParameters();
			getByTitolo().setString(1, titolo);
			ResultSet rs = getByTitolo().executeQuery();
			
			if(rs.next()) {
				f= new Film();
				f.setId(rs.getInt("id"));
				f.setTitolo(titolo);
				f.setRegista(rs.getString("regista"));
				f.setGenere(rs.getString("genere"));
				f.setPosti(rs.getInt("posti"));
				f.setOraInizio(rs.getTime("oraInizio"));
				f.setDescrizione(rs.getString("descrizione"));
				
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return f;
	}
	
	
	public List<Film> getAll(){
		List<Film> result = new ArrayList<Film>();
		try {
			ResultSet rs = getConnection().createStatement().executeQuery("select * from film");
			while(rs.next()) {
				Film f = new Film();
				f.setId(rs.getInt("id"));
				f.setTitolo(rs.getString("titolo"));
				f.setRegista(rs.getString("regista"));
				f.setGenere(rs.getString("genere"));
				f.setPosti(rs.getInt("posti"));
				f.setOraInizio(rs.getTime("oraInizio"));
				f.setDescrizione(rs.getString("descrizione"));
				result.add(f);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		if(connection == null) {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(CONNECTION_STRING);
		}
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public PreparedStatement getByTitolo() throws ClassNotFoundException, SQLException {
		if(byTitolo == null) {
			byTitolo = getConnection().prepareStatement("select * from film where titolo=?");
		}
			
		return byTitolo;
	}

	public void setByTitolo(PreparedStatement byTitolo) {
		this.byTitolo = byTitolo;
	}


	public PreparedStatement getInsert() throws ClassNotFoundException, SQLException {
		if(insert == null) {
			insert = getConnection().prepareStatement("Insert into film (titolo, regista, genere, posti, oraInizio, descrizione) values (?,?,?,?,?,?)");
		}
		return insert;
	}


	public void setInsert(PreparedStatement insert) {
		this.insert = insert;
	}


	public PreparedStatement getUpdate() throws ClassNotFoundException, SQLException {
		if(update == null) {
			update = getConnection().prepareStatement("Update film set titolo=?, regista=?, genere=?, posti=?, oraInizio=?, descrizione=? where id=? ");
		}
		return update;
	}


	public void setUpdate(PreparedStatement update) {
		this.update = update;
	}
	
	
}

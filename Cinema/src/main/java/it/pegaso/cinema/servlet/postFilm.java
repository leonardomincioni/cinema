package it.pegaso.cinema.servlet;

import java.io.IOException;
import java.sql.Time;
import java.time.LocalTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.pegaso.cinema.dao.FilmDao;
import it.pegaso.cinema.model.Film;

/**
 * Servlet implementation class postFilm
 */
public class postFilm extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public postFilm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FilmDao dao = new FilmDao();
		String titolo = request.getParameter("titolo");
		Film film = dao.filmByTitolo(titolo);
		
		if(film == null) {
			Film nuovoFilm = new Film();
			
			nuovoFilm.setTitolo(titolo);
			nuovoFilm.setRegista(request.getParameter("regista"));
			nuovoFilm.setGenere(request.getParameter("genere"));
			nuovoFilm.setPosti(Integer.parseInt(request.getParameter("posti")));
			nuovoFilm.setOraInizio(Time.valueOf(request.getParameter("oraInizio")));
			nuovoFilm.setDescrizione(request.getParameter("descrizione"));
			
			dao.insert(nuovoFilm);
		} else {
			film.setTitolo(titolo);
			film.setRegista(request.getParameter("regista"));
			film.setGenere(request.getParameter("genere"));
			film.setPosti(Integer.parseInt(request.getParameter("posti")));
			film.setOraInizio(Time.valueOf(request.getParameter("oraInizio")));
			film.setDescrizione(request.getParameter("descrizione"));
			dao.update(film);
		}
		RequestDispatcher rd = request.getRequestDispatcher("modifica.jsp");
		rd.forward(request, response);
	}

}

package it.pegaso.cinema.servlet;

import java.io.IOException;
import java.time.LocalTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AcquistaBiglietti
 */
public class AcquistaBiglietti extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AcquistaBiglietti() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String orario = request.getParameter("orario");
		String posti = request.getParameter("posti");
		String esito = "";
		
		int numPosti = Integer.parseInt(posti);
		LocalTime ora = LocalTime.parse(orario);
		LocalTime adesso = LocalTime.now();
		if(numPosti > 0 && 
				ora.isAfter(adesso)) {
			esito = String.format("Acquisto riuscito, il film inizia alle %s", orario);
		} else {
			if (numPosti == 0) {
				esito = "Biglietti non disponibili, posti esauriti";
			} else {
				esito = "Biglietti non disponibili, film gi� iniziato";
			}
			
		}
		request.setAttribute("esito", esito);
		RequestDispatcher rd = request.getRequestDispatcher("esito.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

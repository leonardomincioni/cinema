<%@page import="it.pegaso.cinema.model.Film"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body style="color: gold; background: DarkBlue">
<% Film f = (Film) request.getAttribute("film"); %>

<h1 style="text-align: center; color: gold"><%=f.getTitolo() %></h1>
<h3>Sinossi: </h3>
<p><%=f.getDescrizione() %></p>
<h3>Orario: </h3><p><%=f.getOraInizio() %></p>
<a href="AcquistaBiglietti?orario=<%=f.getOraInizio() %>&posti=<%=f.getPosti() %>"><input type="button" value="Acquista biglietti"></a>
</body>
</html>
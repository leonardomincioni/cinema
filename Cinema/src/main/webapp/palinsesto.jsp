<%@page import="it.pegaso.cinema.model.Film"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Tutti i film</title>
</head>
<body style="background-color: Darkblue">
	
	<% List<Film> list = (List<Film>) request.getAttribute("lista"); 
	if(list.size()>0){
	
	%>
	
	<h1 style="color: gold; text-align: center;">Programma di oggi</h1>
	<table align="center" style="padding-top: 10px; color: gold">
		<thead>
			<tr>
				<th style="width: 20em; border: 1px solid white"> Titolo </th>
				<th style="width: 20em; border: 1px solid white"> Regista </th>
				<th style="width: 20em; border: 1px solid white"> Genere </th>
			</tr>
		</thead>
		<tbody>
		<% for (Film f : list){ %>
			<tr>
				<td style="width: 20em; border: 1px solid white; text-align: center"><a style="color: gold" href="GetFilmInfo?titolo=<%= f.getTitolo()%>"><%= f.getTitolo() %></a></td>
				<td style="width: 20em; border: 1px solid white; text-align: center"><%= f.getRegista() %></td>
				<td style="width: 20em; border: 1px solid white; text-align: center"><%= f.getGenere() %></td>
			</tr>
			<%} %>
		</tbody>
	</table>
	<% } else { %>
		<h1 style="color: gold; text-align: center;">Programma di oggi</h1>
	<%} %>
</body>
</html>